rust-grcov (0.8.12-2) unstable; urgency=medium

  * Team upload.
  * Package grcov 0.8.12 from crates.io using debcargo 2.6.0
  * Stop patching zip dependency.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 28 Dec 2022 10:19:29 +0000

rust-grcov (0.8.12-1) unstable; urgency=medium

  * Team upload.
  * Package grcov 0.8.12 from crates.io using debcargo 2.6.0
    + Version 0.8.13 requires the "infer" crate which is not in Debian.
  * Drop quick-xml-0.22.diff, upstream now depends on quick-xml 0.25
  * Bump quick-xml dependency to 0.26
  * Drop bump-symbolic.diff, upstream now depends on symbolic-* 9.2
  * Update overridden debian/tests/control.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 22 Dec 2022 21:03:01 +0000

rust-grcov (0.8.11-2) unstable; urgency=medium

  * Team upload.
  * Package grcov 0.8.11 from crates.io using debcargo 2.5.0
  * Remove demangle-with-swift feature, since rust-symbolic-demangle is built
    without the swift feature.
  * Remove tcmalloc feature until rust-tcmalloc is installable.
  * Override the generated d/tests/control to test the features which make
    sense (default, all features, and demangle-no-swift)
  * Re-enable generation of man page, now that upstream's --help works
    properly

 -- James McCoy <jamessan@debian.org>  Mon, 05 Sep 2022 22:26:05 -0400

rust-grcov (0.8.11-1) unstable; urgency=medium

  [ Peter Michael Green ]
  * Team upload.
  * Package grcov 0.8.9 from crates.io using debcargo 2.5.0
  * Package grcov 0.8.11 from crates.io using debcargo 2.5.0 (Closes: 1017072)
  * Update patches for new upstream and current package situation in Debian.
  * Disable tests which rely on test data that is not included in the crates.io
    release.

  [ Sylvestre Ledru ]
  * Package grcov 0.8.2 from crates.io using debcargo 2.4.4-alpha.0
  * Package grcov 0.8.10 from crates.io using debcargo 2.5.0

 -- Peter Michael Green <plugwash@debian.org>  Sat, 20 Aug 2022 21:07:38 +0200

rust-grcov (0.5.15-2) unstable; urgency=medium

  * Package grcov 0.5.15 from crates.io using debcargo 2.4.3
  * Disable dwz. See https://github.com/rust-lang/rust/issues/66118

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 15 Dec 2020 10:30:10 +0100

rust-grcov (0.5.15-1) unstable; urgency=medium

  * Package grcov 0.5.15 from crates.io using debcargo 2.4.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 27 Nov 2020 23:48:04 +0100

rust-grcov (0.4.1-3) unstable; urgency=medium

  * Package grcov 0.4.1 from crates.io using debcargo 2.4.3
  * Fix the dep (Closes: #948630)
  * Cherry pick cb0f93e69e652ef7d45c2e42157af100935d705f
    for crossbeam 0.7

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 27 Nov 2020 22:13:55 +0100

rust-grcov (0.4.1-2) unstable; urgency=medium

  * Team upload.
  * Package grcov 0.4.1 from crates.io using debcargo 2.4.2

  [ Sylvestre Ledru ]
  * Package grcov 0.5.7 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Wed, 08 Jan 2020 22:52:51 +0000

rust-grcov (0.4.1-1) unstable; urgency=medium

  * Package grcov 0.4.1 from crates.io using debcargo 2.2.10
  * Disable the auto generation of the manpage

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 22 Jan 2019 15:31:56 +0100

rust-grcov (0.3.0-1) unstable; urgency=medium

  * Package grcov 0.3.0 from crates.io using debcargo 2.2.10
    (Closes: #910358)

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 21 Jan 2019 13:32:11 +0100
