Description: updates for newer dependencies
 Update up to upstream commit 51689723
 Update version to current patch version of the deps in Debian
Author: Marc Dequènes (Duck) <Duck@DuckCorp.org>
Origin: upstream
Forwarded: not-needed
Last-Update: 2022-09-20
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -1,3 +1,12 @@
+## Unreleased
+* Improve lifetime flexibility for `Font::glyphs_for` & `Font::layout`.
+* Update owned_ttf_parser -> `0.15`.
+* Update ab_glyph_rasterizer => `0.1.5`.
+* Update crossbeam-queue -> `0.8`.
+* Update crossbeam-utils -> `0.8`.
+* Update num_cpus => `1.13`.
+* Update approx => `0.5`.
+
 ## 0.9.2
 * Update ttf-parser -> `0.6`.
 * Use more flexible lifetime bounds for `Font::layout`.
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -26,7 +26,7 @@
 [package.metadata.docs.rs]
 features = ["gpu_cache"]
 [dependencies.ab_glyph_rasterizer]
-version = "0.1.1"
+version = "0.1.7"
 default-features = false
 
 [dependencies.libm]
@@ -39,14 +39,14 @@
 optional = true
 
 [dependencies.owned_ttf_parser]
-version = "0.6"
+version = "0.15.2"
 default-features = false
 
 [dependencies.rustc-hash]
 version = "1"
 optional = true
 [dev-dependencies.approx]
-version = "0.3"
+version = "0.5"
 default-features = false
 
 [features]
@@ -56,13 +56,13 @@
 libm-math = ["libm", "ab_glyph_rasterizer/libm"]
 std = ["has-atomics", "owned_ttf_parser/default", "ab_glyph_rasterizer/default"]
 [target."cfg(not(target_arch = \"wasm32\"))".dependencies.crossbeam-deque]
-version = "0.7"
+version = "0.8"
 optional = true
 
 [target."cfg(not(target_arch = \"wasm32\"))".dependencies.crossbeam-utils]
-version = "0.7"
+version = "0.8"
 optional = true
 
 [target."cfg(not(target_arch = \"wasm32\"))".dependencies.num_cpus]
-version = "1.0"
+version = "1.13"
 optional = true
--- a/src/font.rs
+++ b/src/font.rs
@@ -29,8 +29,8 @@
 /// ```
 #[derive(Clone)]
 pub enum Font<'a> {
-    Ref(Arc<owned_ttf_parser::Font<'a>>),
-    Owned(Arc<owned_ttf_parser::OwnedFont>),
+    Ref(Arc<owned_ttf_parser::Face<'a>>),
+    Owned(Arc<owned_ttf_parser::OwnedFace>),
 }
 
 impl fmt::Debug for Font<'_> {
@@ -51,7 +51,7 @@
     ///
     /// Returns `None` for invalid data.
     pub fn try_from_bytes_and_index(bytes: &[u8], index: u32) -> Option<Font<'_>> {
-        let inner = Arc::new(owned_ttf_parser::Font::from_data(bytes, index)?);
+        let inner = Arc::new(owned_ttf_parser::Face::from_slice(bytes, index).ok()?);
         Some(Font::Ref(inner))
     }
 
@@ -66,18 +66,18 @@
     ///
     /// Returns `None` for invalid data.
     pub fn try_from_vec_and_index(data: Vec<u8>, index: u32) -> Option<Font<'static>> {
-        let inner = Arc::new(owned_ttf_parser::OwnedFont::from_vec(data, index)?);
+        let inner = Arc::new(owned_ttf_parser::OwnedFace::from_vec(data, index).ok()?);
         Some(Font::Owned(inner))
     }
 }
 
 impl<'font> Font<'font> {
     #[inline]
-    pub(crate) fn inner(&self) -> &owned_ttf_parser::Font<'_> {
-        use owned_ttf_parser::AsFontRef;
+    pub(crate) fn inner(&self) -> &owned_ttf_parser::Face<'_> {
+        use owned_ttf_parser::AsFaceRef;
         match self {
             Self::Ref(f) => f,
-            Self::Owned(f) => f.as_font(),
+            Self::Owned(f) => f.as_face_ref(),
         }
     }
 
@@ -100,9 +100,7 @@
 
     /// Returns the units per EM square of this font
     pub fn units_per_em(&self) -> u16 {
-        self.inner()
-            .units_per_em()
-            .expect("Invalid font units_per_em")
+        self.inner().units_per_em()
     }
 
     /// The number of glyphs present in this font. Glyph identifiers for this
@@ -137,7 +135,7 @@
     /// points or glyph ids produced by the given iterator `itr`.
     ///
     /// This is equivalent in behaviour to `itr.map(|c| font.glyph(c))`.
-    pub fn glyphs_for<I: Iterator>(&self, itr: I) -> GlyphIter<'_, I>
+    pub fn glyphs_for<'a, I: Iterator>(&'a self, itr: I) -> GlyphIter<'a, 'font, I>
     where
         I::Item: IntoGlyphId,
     {
@@ -177,25 +175,25 @@
     /// # let (scale, start) = (Scale::uniform(0.0), point(0.0, 0.0));
     /// # let font: Font = unimplemented!();
     /// font.glyphs_for("Hello World!".chars())
-    ///     .scan((None, 0.0), |&mut (mut last, mut x), g| {
+    ///     .scan((None, 0.0), |(last, x), g| {
     ///         let g = g.scaled(scale);
     ///         if let Some(last) = last {
-    ///             x += font.pair_kerning(scale, last, g.id());
+    ///             *x += font.pair_kerning(scale, *last, g.id());
     ///         }
     ///         let w = g.h_metrics().advance_width;
-    ///         let next = g.positioned(start + vector(x, 0.0));
-    ///         last = Some(next.id());
-    ///         x += w;
+    ///         let next = g.positioned(start + vector(*x, 0.0));
+    ///         *last = Some(next.id());
+    ///         *x += w;
     ///         Some(next)
     ///     })
     /// # ;
     /// ```
-    pub fn layout<'f, 's>(
-        &'f self,
+    pub fn layout<'a, 's>(
+        &'a self,
         s: &'s str,
         scale: Scale,
         start: Point<f32>,
-    ) -> LayoutIter<'f, 's> {
+    ) -> LayoutIter<'a, 'font, 's> {
         LayoutIter {
             font: self,
             chars: s.chars(),
@@ -220,13 +218,16 @@
             let hscale = self.scale_for_pixel_height(scale.y);
             hscale * (scale.x / scale.y)
         };
-        let kern = self
-            .inner()
-            .kerning_subtables()
-            .filter(|st| st.is_horizontal() && !st.is_variable())
-            .filter_map(|st| st.glyphs_kerning(first_id, second_id))
-            .next()
-            .unwrap_or(0);
+
+        let kern = if let Some(kern) = self.inner().tables().kern {
+            kern.subtables
+                .into_iter()
+                .filter(|st| st.horizontal && !st.variable)
+                .find_map(|st| st.glyphs_kerning(first_id, second_id))
+                .unwrap_or(0)
+        } else {
+            0
+        };
 
         factor * f32::from(kern)
     }
--- a/src/gpu_cache.rs
+++ b/src/gpu_cache.rs
@@ -997,7 +997,7 @@
         let font_data = include_bytes!("../dev/fonts/wqy-microhei/WenQuanYiMicroHei.ttf");
         let font = Font::try_from_bytes(font_data as &[u8]).unwrap();
 
-        let mut cache = Cache::builder()
+        let mut cache: Cache<'static> = Cache::builder()
             .dimensions(32, 32)
             .scale_tolerance(0.1)
             .position_tolerance(0.1)
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -528,29 +528,29 @@
 }
 
 #[derive(Clone)]
-pub struct GlyphIter<'b, I: Iterator>
+pub struct GlyphIter<'a, 'font, I: Iterator>
 where
     I::Item: IntoGlyphId,
 {
-    font: &'b Font<'b>,
+    font: &'a Font<'font>,
     itr: I,
 }
 
-impl<'b, I> Iterator for GlyphIter<'b, I>
+impl<'a, 'font, I> Iterator for GlyphIter<'a, 'font, I>
 where
     I: Iterator,
     I::Item: IntoGlyphId,
 {
-    type Item = Glyph<'b>;
+    type Item = Glyph<'font>;
 
-    fn next(&mut self) -> Option<Glyph<'b>> {
+    fn next(&mut self) -> Option<Glyph<'font>> {
         self.itr.next().map(|c| self.font.glyph(c))
     }
 }
 
 #[derive(Clone)]
-pub struct LayoutIter<'font, 's> {
-    font: &'font Font<'font>,
+pub struct LayoutIter<'a, 'font, 's> {
+    font: &'a Font<'font>,
     chars: core::str::Chars<'s>,
     caret: f32,
     scale: Scale,
@@ -558,7 +558,7 @@
     last_glyph: Option<GlyphId>,
 }
 
-impl<'font, 's> Iterator for LayoutIter<'font, 's> {
+impl<'a, 'font, 's> Iterator for LayoutIter<'a, 'font, 's> {
     type Item = PositionedGlyph<'font>;
 
     fn next(&mut self) -> Option<PositionedGlyph<'font>> {
@@ -577,11 +577,11 @@
 
 pub(crate) trait NearZero {
     /// Returns if this number is kinda pretty much zero.
-    fn is_near_zero(self) -> bool;
+    fn is_near_zero(&self) -> bool;
 }
 impl NearZero for f32 {
     #[inline]
-    fn is_near_zero(self) -> bool {
+    fn is_near_zero(&self) -> bool {
         self.abs() <= core::f32::EPSILON
     }
 }
